package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class loginPO {

	@FindBy()
	protected WebElement userName;
	
	@FindBy()
	protected WebElement passWord;
	
	@FindBy(xpath="(//a[contains(text(),'Log In')])")
	public WebElement loginbutton;
	
	WebDriver driver;
	
	public loginPO(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void clickonLogin(){
		loginbutton.click();
	}
	
	public void clickOnLogin(){
		this.clickonLogin();
	}
}
